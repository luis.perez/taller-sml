# Frontend SML

Ejercicio de frontend para Spiral Media Labs

## Requerimientos

- Ruby 2.3.3
- NodeJS 6.9.1
- Grunt 1.0

## Instalación

```
# instalar gemas
$ bundle install

# instalar paquetes npm
$ npm install

# instalar hooks con overcommit
$ overcommit --install
$ overcommit --sign
```
